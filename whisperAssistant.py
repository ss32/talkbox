# Load model directly
from transformers import WhisperProcessor, WhisperForConditionalGeneration
import pyaudio
import numpy as np
import torch
from langchain.prompts import PromptTemplate
from langchain.llms import Ollama
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.chains import LLMChain
import os

def play_sound(file_path: str):
    cmd = f"ffplay -v 0 -nodisp -autoexit {file_path}"
    os.system(cmd)

def normalize(x: np.array) -> np.array:
    n = (x - x.min()) / (x.max() - x.min())
    return ((n - 0.5) * 2).astype(np.float32)

def generate_story(scenario: str) -> str:
    template = """
    You are a helpful personal assistant;
    You provide succinct answers to requests for information;
    
    CONTEXT: {scenario}"""

    prompt = PromptTemplate(template=template, input_variables=["scenario"])
    pipe = Ollama(model="llama2:13b", 
             callback_manager = CallbackManager([StreamingStdOutCallbackHandler()]))

    story_llm = LLMChain(
        llm=pipe,
        prompt=prompt,
        verbose=True,
    )

    return story_llm.predict(scenario=scenario)

def get_audio(stream, fs: int, chunk: int, seconds: int):
    audio = np.array([])
    stream.start_stream()
    for _ in range(0, int(fs / chunk * seconds)):
        data = stream.read(chunk, exception_on_overflow=False)
        audio = np.hstack((audio, np.frombuffer(data, dtype=np.int32)))
    stream.stop_stream()
    output = {}
    output["sampling_rate"] = fs
    output["array"] = normalize(audio)
    return output

def get_transcription(model, input_features) -> str:
    predicted_ids = model.generate(input_features)
    transcription = processor.batch_decode(predicted_ids, skip_special_tokens=True)
    return transcription[0]

def transcribe_audio(processor, model, stream: pyaudio.PyAudio.Stream, fs: int, chunk: int, seconds: int):
    output = get_audio(stream, fs, chunk, seconds)
    input_features = processor(
       output["array"], sampling_rate=output["sampling_rate"], return_tensors="pt"
    ).input_features.to(device)
    transcription = get_transcription(model, input_features)
    magic_word = "hey computer"
    if magic_word.lower() in transcription.lower():
        play_sound('beep.mp3')
        output = get_audio(stream, fs, chunk, 10)
        input_features = processor(
            output["array"], sampling_rate=output["sampling_rate"], return_tensors="pt"
        ).input_features.to(device)
        request = get_transcription(model, input_features)
        recipe = generate_story(request)
        print(recipe)
        print('\n\n Ready for another request\n\n')


device = "cuda:0" if torch.cuda.is_available() else "cpu"
processor = WhisperProcessor.from_pretrained("openai/whisper-large-v2")
model = WhisperForConditionalGeneration.from_pretrained("openai/whisper-large-v2").to(device)
model.config.forced_decoder_ids = processor.get_decoder_prompt_ids(language="english", task="transcribe")
chunk = 1024 * 2  # Record in chunks of 1024 samples
sample_format = pyaudio.paInt16 # 16 bits per sample
channels = 2
fs = 16000  # Record at 16000 samples per second
seconds = 3
audio = pyaudio.PyAudio()
stream = audio.open(
    format=sample_format,
    channels=channels,
    rate=fs,
    frames_per_buffer=chunk,
    input=True,
)
while True:
    try:
        transcribe_audio(processor, model, stream, fs, chunk, seconds)
    except KeyboardInterrupt:
        print("\nClosing")
        audio.terminate()
        exit(0)